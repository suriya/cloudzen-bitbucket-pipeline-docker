Docker image to run the bookshelf project by [CloudZen Software
Labs](https://www.cloudzen.in) on Bitbucket Pipeline

Our Docker image starts with a base Ubuntu image, installs Postgresql,
Python, and other applications, and finally installs Python packages that
our projects need.

Our image is available at [cloudzen/bookshelf-bitbucket-pipeline-docker](https://hub.docker.com/r/cloudzen/bookshelf-bitbucket-pipeline-docker/).
