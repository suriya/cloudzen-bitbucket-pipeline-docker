Docker image to run the gstcomply project by [GSTZen
](https://www.gstzen.in) on Bitbucket Pipeline

GSTZen provides GST Return Filing, Reconciliation and Reports.

Our Docker image starts with a base Ubuntu image, installs Postgresql,
Python, and other applications, and finally installs Python packages that
our projects need.

Our image is available at [cloudzen/gstcomply-bitbucket-pipeline-docker](https://hub.docker.com/r/cloudzen/gstcomply-bitbucket-pipeline-docker/).
