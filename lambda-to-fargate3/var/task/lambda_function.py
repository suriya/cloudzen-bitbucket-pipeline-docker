
from __future__ import absolute_import, unicode_literals, print_function

"""
Our entry point.

This file/function exists within our Docker image. The docker image is
based off of base image lambci/lambda:python2.7 https://hub.docker.com/r/lambci/lambda/

The base image has two files: bootstrap.py and runtime.py which do heavy
lifing and call `lambda_handler()` (defined below) with appropirate
parameters.

bootstrap.py: https://gist.github.com/alanjds/000b15f7dcd43d7646aab34fcd3cef8c#file-awslambda-bootstrap-py

Could not find runtime.py
sudo docker run -it --rm --entrypoint /bin/cat lambci/lambda:python2.7 /var/runtime/awslambda/runtime.py

Some parts of runtime.py are pasted here. (to know what environment variables we can define).

    _GLOBAL_HANDLER = sys.argv[1] if len(sys.argv) > 1 else os.environ.get('AWS_LAMBDA_FUNCTION_HANDLER',
            os.environ.get('_HANDLER', 'lambda_function.lambda_handler'))
    _GLOBAL_EVENT_BODY = sys.argv[2] if len(sys.argv) > 2 else os.environ.get('AWS_LAMBDA_EVENT_BODY',
            (sys.stdin.read() if os.environ.get('DOCKER_LAMBDA_USE_STDIN', False) else '{}'))
    _GLOBAL_FCT_NAME = os.environ.get('AWS_LAMBDA_FUNCTION_NAME', 'test')
    _GLOBAL_VERSION = os.environ.get('AWS_LAMBDA_FUNCTION_VERSION', '$LATEST')
    _GLOBAL_MEM_SIZE = os.environ.get('AWS_LAMBDA_FUNCTION_MEMORY_SIZE', '1536')
    _GLOBAL_TIMEOUT = int(os.environ.get('AWS_LAMBDA_FUNCTION_TIMEOUT', '300'))
    _GLOBAL_REGION = os.environ.get('AWS_REGION', os.environ.get('AWS_DEFAULT_REGION', 'us-east-1'))
    _GLOBAL_ACCOUNT_ID = os.environ.get('AWS_ACCOUNT_ID', _random_account_id())
    _GLOBAL_ACCESS_KEY_ID = os.environ.get('AWS_ACCESS_KEY_ID', 'SOME_ACCESS_KEY_ID')
    _GLOBAL_SECRET_ACCESS_KEY = os.environ.get('AWS_SECRET_ACCESS_KEY', 'SOME_SECRET_ACCESS_KEY')
    _GLOBAL_SESSION_TOKEN = os.environ.get('AWS_SESSION_TOKEN', None)

"""

import logging
import os
import io
import importlib
import sys
import urllib2
import zipfile
import boto3

SOURCE_EXTRACTION_PATH = '/tmp/lambda-contents'

def lambda_handler(event, context):
    logging.debug('In lambda_handler within ECS Fargate')
    logging.debug('Function name', context.invoked_function_arn)
    logging.debug('Event: {}'.format(event))
    # We want to get credentials from ECS based on the IAM Role
    # However, this docker image's bootstrap code sets up these environment
    # variables and these environment variables take precedence in deciding
    # the credentials
    # https://boto3.amazonaws.com/v1/documentation/api/latest/guide/configuration.html#configuring-credentials
    # Therefore, we clear environment variables related to credentails
    for evname in ['AWS_ACCESS_KEY_ID', 'AWS_SECRET_ACCESS_KEY', 'AWS_SESSION_TOKEN']:
        os.environ.pop(evname, None)
    client = boto3.client('lambda')
    response = client.get_function(FunctionName=context.invoked_function_arn)
    zip_url = response['Code']['Location']
    zip_contents = urllib2.urlopen(zip_url).read()
    os.makedirs(SOURCE_EXTRACTION_PATH)
    zipfile.ZipFile(io.BytesIO(zip_contents), 'r').extractall(SOURCE_EXTRACTION_PATH)
    sys.path.insert(0, SOURCE_EXTRACTION_PATH)
    handler_name = response['Configuration']['Handler']
    parts = handler_name.split('.')
    module_path = '.'.join(parts[:-1])
    fn_name = parts[-1]
    module = importlib.import_module(module_path)
    fn = getattr(module, fn_name)
    return fn(event, context)
