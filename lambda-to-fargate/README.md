Thin Docker container that runs long-running Lambda tasks on AWS Fargate.

AWS Lambda has a 3 GB RAM limit and 15 minute execution time limit. Fargate
provides more RAM and no time limit.

We follow the method suggested in 
https://kalinchernev.github.io/solving-aws-lambda-timeouts-fargate/

We create a thin Fargate handler than downloads the Lambda Zip file and
runs the Lambda handler code (within the extracted Zip). This lambda
handler will work exactly as it does on Lambda, except it will not have a
15 minute limit.
