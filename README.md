Docker image to run projects by https://www.cloudzen.in on Bitbucket Pipeline

Our Docker image starts with a base Ubuntu image, installs Postgresql,
Python, and other applications, and finally installs Python packages that
our projects need.

Our image is available at [cloudzen/cloudzen-bitbucket-pipeline-docker](https://hub.docker.com/r/cloudzen/cloudzen-bitbucket-pipeline-docker/).
